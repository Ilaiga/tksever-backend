$(document).ready(function () {
	$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

	$('.js-prices-save-button').on('click', function() {
		$(document).trigger('formSubmit');
	});
	$(document).on('formSubmit', function () {
		$('.js-delivery-methods-json').val(getJsonDeliveryMethods());
		$('.js-header-blocks-json').val(getJsonHeaderBlocks());
		$('.js-mark-lists-json').val(getJsonMarkLists());
	});
    ////////////////////////////
	//table builder/////////////
	////////////////////////////

	if ($.fn.tagsInput && $('.js-thead').length) {
		let $table = $('.js-table-builder');
		buildTable();
		$('.glyphicon').css('color', 'white');
		$('.js-table-separator > td:first-child').css('text-align', 'center');
		function buildTable() {
			let json = $('.js-table-json').val();
			if(json) {
				let table = JSON.parse(json);
				let $trTh = $('<tr>', {class: 'js-table-builder-row'});
				for(let field in table.head) {
					$('<td>', {text: table.head[field]}).appendTo($trTh);
				}
				$trTh.appendTo($table);
				for(let row in table.body) {
					let $tr = $('<tr>', {class: 'js-table-builder-row'});
					for(let col in table.body[row]['cols']) {
						$('<td>', {text: table.body[row]['cols'][col], rowspan: table.body[row]['rowspan'][col]}).appendTo($tr);
					}
					if(table.body[row]['type'] == 'separator') {
						$tr.addClass('js-table-separator').find('td').attr('colspan', table.head.length);
					}
					$tr.appendTo($table);
				}
				$('input.js-thead').val(table.head.join(','));
			}
			$('.js-table-builder').tableEdid({
				stubElements: '<a class="btn btn-success btn-xs addCol" href="javascript://" role="button"><span class="glyphicon glyphicon-plus-sign"></span></a>',
				topControlsElements: '<a class="btn btn-success btn-xs addCol" href="javascript://" role="button"><span class="glyphicon glyphicon-plus-sign"></span></a>' +
					'<a class="btn btn-danger btn-xs delCol" href="javascript://" role="button"><span class="glyphicon glyphicon-minus-sign"></span></a>',
				rowControlsElements: '<a class="btn btn-success btn-xs addrow" href="javascript://" role="button"><span class="glyphicon glyphicon-plus-sign"></span></a>' +
					'<a class="btn btn-danger btn-xs delrow" href="javascript://" role="button"><span class="glyphicon glyphicon-minus-sign"></span></a>',
				compileTableAfter: function() {
					this.$table.addClass('table table-bordered table-hover tableEdit');
					return true;
				},
			});
			tableRestruction($('.tableEdit'));
			$(document).on('cell:editing:stop', function () {
				$(document).trigger('changeTable');
			});
			$(document).on('table:add:col', function () {
				$(document).trigger('changeTable');
			});
			$(document).on('table:del:col', function () {
				$(document).trigger('changeTable');
			});
		}

		function tableRestruction($table) {
			$('.glyphicon').css('color', 'white');
			$('.js-table-separator').css('text-align', 'center');
			let $tbody = $table.find('tbody');
			let cols = $tbody.find('tr:first-child > td:not(:last-child)').length + $tbody.find('tr:first-child > th').length;
			$tbody.find('tr:first').find('td:not(:last-child)').each(function () {
				renameElement($(this),'<th>').empty();
			});
			$tbody.find('tr:not(:first-child)').each(function() {
				let $tr = $(this);
				if($tr.find('td').length < cols) {
					$tr.addClass('js-table-separator');
					$tr.find('td:not(:first-child):not(:last-child)').remove();
					$tr.find('td:first-child').attr('colspan', cols);
				}
			});
		}

		function getJsonTable() {
			let $table = $('.tableEdit');
			let $tbody = $table.find('tbody');
			let table = {
				'head': [],
				'body': []
			};
			$tbody.find('tr:first > th:not(:last-child)').each(function () {
				let val = $(this).html();
				table.head.push(val);
			});
			$tbody.find('tr:not(:first-child)').each(function () {
				let row = {
					'cols': [],
					'type': 'row',
					'rowspan': []
				};
				$(this).find('td:not(:last-child)').each(function () {
					let val = $(this).html();
					row.cols.push(val);
					row.rowspan.push($(this).attr('rowspan'));
				});
				if($(this).hasClass('js-table-separator')) {
					row.type = 'separator';
				}
				table.body.push(row);
			});
			return JSON.stringify(table);
		}

		$(document).on('changeTable', function () {
			tableRestruction($('.tableEdit'));
			$('input.js-table-json').val(getJsonTable());
		});
	}

	//////////////////////////////
	//deliveryMethods/////////////
	//////////////////////////////
	let $container = $('.js-delivery-methods');
	buildDeliveryMethods();

	$('.js-delivery-method-add').on('click', function() {
		let $row = $('.js-delivery-method:first').clone();
		$row.find('.js-delivery-method-name').val('');
		$row.find('.js-delivery-method-description').val('');
		$container.append($row);
		return false;
	});

	$(document).on('click', '.js-delivery-method-delete', function() {
		$(this).closest('.js-delivery-method').remove();
		return false;
	});

	function buildDeliveryMethods() {
		let json = $('.js-delivery-methods-json').val();
		if(json != '[]') {
			let rows = JSON.parse(json);
			let $row_tmp = $('.js-delivery-method').detach();
			for(let row in rows) {
				let $row = $row_tmp.clone();
				$row.find('.js-delivery-method-name').val(rows[row].name);
				$row.find('.js-delivery-method-description').val(rows[row].description);
				$container.append($row);
			}
		}
	}

	function getJsonDeliveryMethods() {
		let list = [];
		$container.find('.js-delivery-method').each(function () {
			let row = {'name': '', 'description': ''};
			row.name = $(this).find('.js-delivery-method-name').val();
			row.description = $(this).find('.js-delivery-method-description').val();
			list.push(row);
		});
		return JSON.stringify(list);
	}

	//////////////////////////////
	//headerBlocks////////////////
	//////////////////////////////
	let $blockContainer = $('.js-header-blocks');
	buildHeaderBlocks();

	$('.js-header-block-add').on('click', function() {
		let $row = $('.js-header-block:first').clone();
		$row.find('.js-header-block-img').val('');
		$row.find('.js-header-block-text').val('');
		$blockContainer.append($row);
		return false;
	});

	$(document).on('click', '.js-header-block-delete', function() {
		$(this).closest('.js-header-block').remove();
		return false;
	});

	function buildHeaderBlocks() {
		let json = $('.js-header-blocks-json').val();
		if(json != '[]') {
			let rows = JSON.parse(json);
			let $row_tmp = $('.js-header-block').detach();
			for(let row in rows) {
				let $row = $row_tmp.clone();
				$row.find('.js-header-block-img').val(rows[row].img);
				$row.find('.js-header-block-text').val(rows[row].text);
				$blockContainer.append($row);
			}
		}
	}

	function getJsonHeaderBlocks() {
		let list = [];
		$blockContainer.find('.js-header-block').each(function () {
			let row = {'img': '', 'text': ''};
			row.img = $(this).find('.js-header-block-img').val();
			row.text = $(this).find('.js-header-block-text').val();
			list.push(row);
		});
		return JSON.stringify(list);
	}

	//////////////////////////////
	//markLists////////////////
	//////////////////////////////
	$('.js-mark-list-blocks').find('.js-mark-list-block').each(function() {
		$(this).find('input').css('font-weight', 700);
	});
	buildMarkLists();

	$('.js-mark-list-add').on('click', function() {
		let $row = $(this).closest('div').find('.js-mark-list-block:first').clone();
		$row.find('.js-mark-list-text').val('').css('font-weight', 300);
		$row.insertAfter($(this).closest('div').find('.js-mark-list-block:last'));
		return false;
	});

	$(document).on('click', '.js-mark-list-delete', function() {
		$(this).closest('.js-mark-list-block').remove();
		return false;
	});

	function buildMarkLists() {
		let json = $('.js-mark-lists-json').val();
		if(json) {
			let cols = JSON.parse(json);
			let $row_tmp = $('.js-mark-list-block:first').clone();
			for(let col in cols) {
				let $col = $('.js-mark-list-blocks').find('.js-mark-list-block-col:eq('+col+')');
				$col.find('.js-mark-list-block').find('input').val(cols[col].head);
				for(let row in cols[col].rows) {
					let $row = $row_tmp.clone();
					$row.find('input').val( cols[col].rows[row]).css('font-weight', 300);
					$row.insertAfter($col.find('.js-mark-list-block:last'));
				}
			}
		}
	}

	function getJsonMarkLists() {
		let list = [];
		$('.js-mark-list-blocks').find('.js-mark-list-block-col').each(function() {
			let col = {
				head: '',
				rows: []
			};
			$(this).find('.js-mark-list-block').each(function (i,elem) {
				let val = $(this).find('input').val();
				if(i === 0) {
					col.head = val;
				} else {
					col.rows.push(val)
				}
			});
			list.push(col);
		});
		return JSON.stringify(list);
	}


});

function renameElement($element,newElement){

	$element.wrap("<"+newElement+">");
	$newElement = $element.parent();

	//Copying Attributes
	$.each($element.prop('attributes'), function() {
		$newElement.attr(this.name,this.value);
	});

	$element.contents().unwrap();

	return $newElement;
}