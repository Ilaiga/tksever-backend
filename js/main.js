"use strict";

//Placeholders for select
$(document).ready(function() {
	$(".js-phone").inputmask("[+]9 (999) 999-99-99", {
		"placeholder": "+7                "
	});
	$('.__category').select2({
		placeholder: 'Выберите категорию',
		dropdownAutoWidth : true,
	});
	$('.__rooms').select2({
		placeholder: 'Выберите сцену'
	});
});

$('.site-menu__arrow').on('click', function(){
	if($(window).width()<992) {
		//$(this).toggleClass('active');
		$(this).closest('.submenu__wrap').find(' >.site-menu__item-look-all').click();
		$(this).closest('.site-menu__item').toggleClass('active');
	}

});

$('.site-menu__item-look-all').on('click', function(){
	$(this).closest('.submenu__wrap').css('height', '100%');
	$(this).css('display', 'none');
});

//Меняем цвет в зависимости от числа в корзине
let cartCount = parseInt($('.js-cart-count').html());

if(cartCount > 0) {
	$(`.js-cart-count`).addClass('_noempty');
}
else {
	$('.js-cart-count').removeClass('_noempty');
}

//Delete preloader
setTimeout(function() {
	$('.preloader').delay(1000).fadeOut('slow');
}, 1000);

// Open mobile menu
$('.hamburger').on('click', function(){
	$('.js-mobile-nav').addClass('opened');
	$('body').addClass('fixed');
});

$('.js-close-menu').on('click', function(){
	$('.js-mobile-nav').removeClass('opened');
	$('body').removeClass('fixed');
})

//Slider

$('.js-partners-slider').slick({
	dots: false,
	arrows: false,
	infinite: true,
	speed: 700,
	slidesToShow: 2,
	swipeToSlide: true,
	centerMode: false,
	variableWidth: true,
	autoplay: false,
	//autoplaySpeed: 4000,
	responsive: [
		{
			breakpoint: 992,
			settings: {
				arrows: false,
				slidesToShow: 5
			}
		},
		{
			breakpoint: 480,
			settings: {
				arrows: false,
				slidesToShow: 2
			}
		}
	]
});

//Advantages mobile accordion

var accordionItem =  document.getElementsByClassName('advantage-item__title');
console.log(accordionItem.length);


function advantagesMobileCollapse(){
	if (breakPoint == 'xx' || breakPoint == 'xs') {
		$('.advantage-item .advantage-item__title').attr('data-toggle', 'collapse');
		$('.advantage-item .advantage-item__title').attr('aria-expanded', 'true');
		$('.advantage-item .section-text').addClass('collapse');
		$('.advantage-item .section-text').attr('data-parent', '#mobileAccordion');

		for(var i = 0; i < accordionItem.length; i++) {
			$(accordionItem[i]).attr('href', $(accordionItem[i]).attr('data-mobCollapseId'));
			$(accordionItem[i]).attr('aria-controls', $(accordionItem[i]).attr('data-mobCollapseId'));
		}

	} else {
		$('.advantage-item .advantage-item__title').removeAttr('data-toggle', 'collapse');
		$('.advantage-item .advantage-item__title').removeAttr('aria-expanded', 'true');
		$('.advantage-item .section-text').removeAttr('data-parent', '#mobileAccordion');
		$('.advantage-item .section-text').removeClass('collapse');
	}
}

$('body').on('resize_xx_once resize_xs_once resize_sm_once resize_md_once resize_lg_once', advantagesMobileCollapse);
advantagesMobileCollapse();


//Map

function initMap() {
	if($("#map").length != 0) {
		var myLatLng = {lat: 55.045567,  lng: 82.902875};


		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: myLatLng,
			disableDefaultUI: true,
			styles: [
				{
					"featureType": "all",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"weight": "2.00"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#9c9c9c"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.text",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"color": "#f2f2f2"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "landscape.man_made",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 45
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#eeeeee"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#7b7b7b"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
						{
							"color": "#46bcec"
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#c8d7d4"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#070707"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				}
			]
		});

		var pin = {
			path: "M46,23.2c0,3.3-0.7,6.5-2,9.5c-5.8,12.7-16.8,26.1-20,29.9c-0.5,0.5-1.3,0.6-1.8,0.1 c-0.1,0-0.1-0.1-0.1-0.1C18.8,58.7,7.8,45.3,2,32.7c-1.3-2.9-2-6.1-2-9.5C0,10.4,10.3,0,23,0S46,10.4,46,23.2L46,23.2z M34.9,23.2 c0-6.6-5.4-12-11.9-12s-11.9,5.4-11.9,12s5.4,12,11.9,12C29.6,35.2,34.9,29.8,34.9,23.2L34.9,23.2z",
			fillColor: '#bd2222',
			fillOpacity: 1,
			scale: 0.8,
			strokeColor: '#bd2222',
			strokeWeight: 1
		};

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: pin,
			title: 'Hello World!'
		});
	};
};

initMap();

if ($('#delivery-map').length) {
	function initDeliveryMap() {
		var map = new google.maps.Map(document.getElementById('delivery-map'), {
			zoom: 6,
			center: {lat: 50.578150,  lng: 136.996646},
			styles: [
				{
					"featureType": "all",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"weight": "2.00"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#9c9c9c"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.text",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"color": "#f2f2f2"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "landscape.man_made",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 45
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#eeeeee"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#7b7b7b"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
						{
							"color": "#46bcec"
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#c8d7d4"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#070707"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				}
			]
		});

		setMarkers(map);
	}

	// Data for the markers consisting of a name, a LatLng and a zIndex for the
	// order in which these markers should display on top of each other.
	var beaches = [
		['Bondi Beach', 50.578150, 136.996646, 4],
		['Coogee Beach', 51.578150, 137.996646, 5],
		['Cronulla Beach', 49.578150, 135.996646, 3],
		['Manly Beach', 50.078150, 136.596646, 2]
	];

	function setMarkers(map) {
		// Adds markers to the map.

		// Marker sizes are expressed as a Size of X,Y where the origin of the image
		// (0,0) is located in the top left of the image.

		// Origins, anchor positions and coordinates of the marker increase in the X
		// direction to the right and in the Y direction down.
		var image = {
			path: "M46,23.2c0,3.3-0.7,6.5-2,9.5c-5.8,12.7-16.8,26.1-20,29.9c-0.5,0.5-1.3,0.6-1.8,0.1 c-0.1,0-0.1-0.1-0.1-0.1C18.8,58.7,7.8,45.3,2,32.7c-1.3-2.9-2-6.1-2-9.5C0,10.4,10.3,0,23,0S46,10.4,46,23.2L46,23.2z M34.9,23.2 c0-6.6-5.4-12-11.9-12s-11.9,5.4-11.9,12s5.4,12,11.9,12C29.6,35.2,34.9,29.8,34.9,23.2L34.9,23.2z",
			fillColor: '#bd2222',
			fillOpacity: 1,
			scale: 0.6,
			strokeColor: '#bd2222',
			strokeWeight: 1
		};
		// Shapes define the clickable region of the icon. The type defines an HTML
		// <area> element 'poly' which traces out a polygon as a series of X,Y points.
		// The final coordinate closes the poly by connecting to the first coordinate.
		var shape = {
			coords: [1, 1, 1, 20, 18, 20, 18, 1],
			type: 'poly'
		};
		for (var i = 0; i < beaches.length; i++) {
			var beach = beaches[i];
			var marker = new google.maps.Marker({
				position: {lat: beach[1], lng: beach[2]},
				map: map,
				icon: image,
				shape: shape,
				title: beach[0],
				zIndex: beach[3]
			});
		}
	}

	initDeliveryMap();
}

//Update header contacts

if ($('.js-header').length) {
	var filterMain = $('.header-contacts-wrap .header-contacts'),
		filterMainWrap = $('.header-contacts-wrap'),
		filterSmWrap = $('.mobile-contacts');


	function replaceCategoryContent(){
		if ((filterSmWrap.html().length === 0)){
			filterSmWrap.append(filterMain);
		}
	}

	function resetCategoryContent(){
		if (!(filterSmWrap.html().length === 0)){
			setTimeout(function () {
				filterMainWrap.append(filterMain);
			},0)
		}
	}

	function updateCategoryContent(){
		if (breakPoint == 'xs' || breakPoint == 'sm' || breakPoint == 'md' || breakPoint == 'lg') {
			resetCategoryContent();
		} else {
			replaceCategoryContent();
		}
	}


	$('body').on('resize_xx_once resize_xs_once resize_sm_once resize_md_once resize_lg_once', updateCategoryContent);
	updateCategoryContent();
}

// Scroll to calculation sectiob

$(".scroll-to").click(function (){
	$('html, body').animate({
		scrollTop: $(".section-target").offset().top
	}, 2000);
});

//Load content
if ($('.js-load-more').length) {
	$('body').on('click', '.js-load-more', function(){
		var url = $(this).attr('data-url'),
			commentsWrap = $(this).closest('.card-delivery').find('.js-load-wrap');

		$.ajax({
			url: url,
			success: function(data){
				commentsWrap.append(data);
			}
		})
	})
};

$('.overlay, .js-close-thanks').click(function(){
	$('.overlay, .thanks').removeClass('_active');
});

$('.services-item:nth-child(4)').click(function(event){
	event.preventDefault();
	$(this).css('display', 'none');
	$('.services-item:first-child').addClass('_active');
	$('.services-item:nth-child(n+5)').addClass('_active');
});

$('.js-search__direction').keyup(function() {
	let $element = $(this),
		searchValue = $element.val(),
		$directions,
		$foundDirections = $([]),
		$direction,
		directionName;
	$directions = $('.js-directions >.site-menu__item');
	console.log($directions);
	if (!searchValue || searchValue.trim() == '') {
		$foundDirections = $directions;
	} else {
		$.each($directions, function(index, item) {
			$direction = $(item);
			directionName = $direction.find(".site-menu__link").text();
			if (
				(directionName && directionName.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1)
			) {
				$foundDirections = $foundDirections.add($direction);
			}
		});
	}
	$directions.addClass("hidden").removeClass('active');
	if ($foundDirections.length > 0) {
		$foundDirections.removeClass('hidden');
		//$(".js-not-found:visible").hide();
	} else {
		//$(".js-not-found:hidden").show();
	}

});
