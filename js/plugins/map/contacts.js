ymaps.ready(function() {
    var contactsMap = new ymaps.Map('contactsMap', {
        center: [56.577694, 36.504569],
        zoom: 9,
        controls: []
    }),
    contactsPlacemark = new ymaps.Placemark(contactsMap.getCenter(), {}, {
        iconLayout: 'default#image',
        iconImageHref: '/images/icons/marker.svg',
        iconImageSize: [28, 38]
    });

    contactsMap.geoObjects.add(contactsPlacemark);
});