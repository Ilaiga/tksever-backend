<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $pageDescription;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	protected function beforeAction($action) {
		$this->setPageTitle();
		$this->setPageDescription();
		return parent::beforeAction($action);
	}

	public function setPageTitle($title = '') {
		if(empty($title)) {
			if($this->getPage()) {
				return $this->pageTitle = $this->getPage()->metaTitle;
			}
		}
		return $this->pageTitle = $title;
	}

	public function setPageDescription($title = '') {
		if(empty($title)) {
			if($this->getPage()) {
				return $this->pageDescription = $this->getPage()->metaDescription;
			}
		}
		return $this->pageDescription = $title;
	}

	public function getPage()
	{
		if(($pageId = Yii::app()->request->getParam('pageId'))) {
			return Pages::model()->findByPk($pageId);
		}
	}

	public function getPageId()
	{
		return $this->getId();
	}

	public function getCurrentPageId()
	{
		return $this->getId();
	}

	public function setBreadCrumbs(array $items = []) {
		return $this->breadcrumbs = $items;
	}
	
	protected function loadRecord($class_name, $pk = 'id') {
		if (class_exists($class_name)) {
			if ($id = (int) Yii::app()->request->getParam($pk)) {
				$record = $class_name::model()->findByPk($id);
			}
		}
		if (!isset($record) || NULL === $record) {
			throw new CHttpException(404, 'Record not found.');
		}
		return $record;
	}
	
	protected function loadRecordByAttribute($class_name, $params) {
		if (class_exists($class_name)) {
			$record = $class_name::model()->findByAttributes($params);
		}
		if (!isset($record) || NULL === $record) {
			throw new CHttpException(404, 'Record not found.');
		}
		return $record;
	}

	public function render($view, $data=null, $return=false) {
		if(!empty($_GET['routePage'])) {
			$page = Pages::model()->findByPk(intval($_GET['pageId']));
			if($page) {
				$this->setPageTitle($page->metaTitle);
				$this->setPageDescription($page->description);
				Yii::app()->clientScript->registerMetaTag($page->seoKeyWords, 'keywords');
				$data = $data ?? [];
				$data = array_merge($data, ['page' => $page]);
			}
		}
		parent::render($view, $data, $return);
	}

	public function getDirections($limit = 10, $showAll = false, $level = null)
	{
		$q = "";
		if(!$showAll) $q .= "and showOnMainPage = 1";
		if($level) $q .= " and level = 2";
		$criteria = new CDbCriteria([
			'condition' => 'active = 1 and id != 1 '.$q,
//			'params' => [':region' => 'region'],
			'limit' => $limit,
			'order' => 'lft'
		]);
		return DeliveryRoutes::model()->findAll($criteria);
	}

	public function getServices($limit = 3)
	{
		$criteria = new CDbCriteria([
			'condition' => 'templateId = :tId and showDeliveryTypeOnMainPage = 1',
			'params' => [':tId' => 6],
			'limit' => $limit,
			'order' => 'lft desc'
		]);
		return Pages::model()->findAll($criteria);
	}

	public function getProjects($limit = 3)
	{
		$criteria = new CDbCriteria([
			'limit' => $limit,
			'order' => 'date desc'
		]);
		return Projects::model()->findAll($criteria);
	}
}