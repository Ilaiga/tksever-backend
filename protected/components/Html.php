<?php

class Html extends CHtml
{
    public static function activeMonthField($model, $attribute, $htmlOptions = [])
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        self::clientChange('change',$htmlOptions);
        return self::activeInputField('month', $model, $attribute, $htmlOptions);
    }
}