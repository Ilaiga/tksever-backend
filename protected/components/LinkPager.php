<?php

class LinkPager extends CLinkPager
{
	const CSS_FIRST_PAGE = 'first';
	const CSS_LAST_PAGE = 'last';
	const CSS_PREVIOUS_PAGE = 'pagination__nav _prev';
	const CSS_NEXT_PAGE = 'pagination__nav _next';
	const CSS_INTERNAL_PAGE = 'pagination__item';
	const CSS_HIDDEN_PAGE = 'hidden';
	const CSS_SELECTED_PAGE = '_active';

	public $header;
	public $footer;
	public $wrapperOpen;
	public $wrapperClose;

	public $previousPageCssClass = self::CSS_PREVIOUS_PAGE;
	public $nextPageCssClass = self::CSS_NEXT_PAGE;
	public $internalPageCssClass = self::CSS_INTERNAL_PAGE;
	public $hiddenPageCssClass = self::CSS_HIDDEN_PAGE;
	public $selectedPageCssClass = self::CSS_SELECTED_PAGE;
	public $maxButtonCount = 5;
	public $htmlOptions = [];

	public function init() {
		$this->header = self::getHeader();
		$this->footer = self::getFooter();
		$this->wrapperOpen = self::getWrapperOpen();
		$this->wrapperClose = self::getWrapperClose();
		$this->htmlOptions = ['class' => 'pagination h-reset-list'];
	}

	protected static function getHeader() {
		return '<nav class="pagination"><ul class="pagination h-reset-list">';
	}
	
	protected static function getFooter() {
		return '</ul></nav>';
	}
	
	protected static function getWrapperOpen() {
		return '';
	}
	
	protected static function getWrapperClose() {
		return '';
	}

	public function run() {
		echo $this->header;
		echo $this->createPrevButton();
		echo $this->wrapperOpen . implode("\n", $this->createPageButtons()) . $this->wrapperClose;
		echo $this->createNextButton();
		echo $this->footer;
	}

	protected function createPrevButton() {
		$currentPage = $this->getCurrentPage(false);
		$previousPageCssClass = self::CSS_PREVIOUS_PAGE;

		if (($page = $currentPage - 1) < 0)
			$page = 0;

		echo <<< endl
		<li class="pagination__item">
			<a href="{$this->createPageUrl($page)}" class="pagination__nav _prev">
				←
			</a>
		</li>
endl;
	}
	
	protected function createNextButton() {
		$pageCount = $this->getPageCount();
		$currentPage = $this->getCurrentPage(false);
		$nextPageCssClass = self::CSS_NEXT_PAGE;

		if (($page = $currentPage + 1) >= $pageCount - 1)
			$page = $pageCount - 1;

		echo <<< endl
		<li class="pagination__item">
			<a href="{$this->createPageUrl($page)}" class="pagination__nav _next">
				→
			</a>
		</li>
endl;
	}

	protected function createPageButtons() {
		if (($pageCount = $this->getPageCount()) <= 1)
			return array();

		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons = array();

		// first page
//		if ($currentPage >= 3) {
//			$buttons[] = $this->createPageButton(1, 0, self::CSS_INTERNAL_PAGE, $currentPage <= 0, false);
//			if ($currentPage > 3) {
//				$buttons[] = $this->createSpacer();
//			}
//		}

		// internal pages
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);
		}

		// last page
//		if (($pageCount - $currentPage - 1) >= 3) {
//			if (($pageCount - $currentPage - 1) > 3) {
//				$buttons[] = $this->createSpacer();
//			}
//			$buttons[] = $this->createPageButton($pageCount, $pageCount - 1, self::CSS_INTERNAL_PAGE, $currentPage >= $pageCount - 1, false);
//		}

		return $buttons;
	}

	protected function createSpacer($label = '...') {
		return '<li class="pagination__item"><a href="#" class="pagination__link">' . $label . '</a></li>';
	}

	protected function createPageButton($label, $page, $class, $hidden, $selected) {
		if ($hidden || $selected)
			$class .= ' ' . ($hidden ? $this->hiddenPageCssClass : '');
		return '<li class="' . $class . '"><a href="' . $this->createPageUrl($page) . '" class="pagination__link'.($selected ? ' '.$this->selectedPageCssClass : '').'"><span class="pagination__link-text">' . $label . '</span></a></li>';
	}

	protected function getPageRange() {
		$currentPage = $this->getCurrentPage();
		$pageCount = $this->getPageCount();

		$beginPage = max(0, $currentPage - (int) ($this->maxButtonCount / 2));
		if (($endPage = $beginPage + $this->maxButtonCount - 1) >= $pageCount) {
			$endPage = $pageCount - 1;
			$beginPage = max(0, $endPage - $this->maxButtonCount + 1);
		}
		return array($beginPage, $endPage);
	}
}