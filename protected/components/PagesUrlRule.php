<?php

class PagesUrlRule extends  CBaseUrlRule
{
	public $connectionID;

	public function createUrl($manager, $route, $params, $ampersand) {
		return false;
	}

	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
		if(empty($rawPathInfo)) {
			$_GET['pageId'] = 1;
			$_GET['routePage'] = true;
		}
		$parts  = explode('/', $rawPathInfo);
		$slug = trim(array_pop($parts));
		$pages = Pages::model()->findAll('slug=:slug', [':slug' => $slug]);
		foreach ($pages as $page) {
			if($this->getParentUrl($page) === '/' . $rawPathInfo && $page->active) {
				$_GET['pageId'] = $page->id;
				if(!empty($page->route)) {
					$_GET['routePage'] = true;
					return $page->route;
				}
				return 'page/view';
			}
		}
		return false;
	}

	private function getParentUrl(Pages $page)
	{
		$ancestors = $page->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentPage) {
			if($parentPage->slug === '/') continue;
			$url[] = $parentPage->slug;
		}
		$url[] = $page->slug;
		return '/' . implode('/', $url);
	}
}