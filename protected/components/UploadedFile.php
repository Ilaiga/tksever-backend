<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 19.04.2019
 * Time: 13:35
 */

class UploadedFile
{
	private static $_files;

	public static function getInstance($model, $attribute)
	{
		return self::getInstancesByName(CHtml::resolveName($model, $attribute));
	}

	public static function getInstancesByName($name)
	{
		if(null===self::$_files)
			self::prefetchFiles();
		$len=strlen($name);
		$results=array();
		foreach(array_keys(self::$_files) as $key) {
			if(0===strncmp(strtolower($key), strtolower($name), $len+1) && self::$_files[$key]->getError()!=UPLOAD_ERR_NO_FILE)
				$results[] = self::$_files[$key];
		}
		return array_shift($results);
	}

	protected static function prefetchFiles()
	{
		self::$_files = array();
		if(!isset($_FILES) || !is_array($_FILES))
			return;

		foreach($_FILES as $class=>$info)
			self::collectFilesRecursive($class, $info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error']);
	}

	protected static function collectFilesRecursive($key, $names, $tmp_names, $types, $sizes, $errors)
	{
		if(is_array($names))
		{
			foreach($names as $item=>$name)
				self::collectFilesRecursive($key.'['.$item.']', $names[$item], $tmp_names[$item], $types[$item], $sizes[$item], $errors[$item]);
		}
		else
			self::$_files[$key] = new CUploadedFile($names, $tmp_names, $types, $sizes, $errors);
	}

}