<?php
return array(
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Пользователь',
        'bizRule' => null,
        'data' => null,
        'children' => array(
            'editProfile',
        ),
    ),
    'editor' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Модератор',
        'children' => array(
            'user',
	        'handleCrm',
	        'cpAccess',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Администратор',
        'children' => array(
            'editor',
	        'handleBlog',
	        'userAdminSettings',
	        'catalogAdminSettings',
	        'handleAdminSettings',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'editProfile' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Редактировать профиль',
        'bizRule' => null,
        'data' => null
    ),
	'handleCrm' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Управлять CRM',
		'bizRule' => null,
		'data' => null
	),
	'cpAccess' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Доступ к админ панеле',
		'bizRule' => null,
		'data' => null
	),
	'handleBlog' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Управление блогом',
		'bizRule' => null,
		'data' => null
	),
	'handleAdminSettings' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Раздел настроек в админке',
		'bizRule' => null,
		'data' => null
	),
	'catalogAdminSettings' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Раздел каталог в админке',
		'bizRule' => null,
		'data' => null
	),
	'userAdminSettings' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Раздел пользователи в админке',
		'bizRule' => null,
		'data' => null
	),
);