<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 18.06.2019
 * Time: 17:55
 */

class DeliveryController extends Controller
{
	protected $page;
	protected $templates = [
		'1' => 'service',
		'35' => 'brand'
	];

	public function actionView($pageId)
	{
		$page = DeliveryRoutes::model()->findByPk($pageId);
		$this->page = $page;
		$this->setPageTitle($page->metaTitle);
		$this->setPageDescription($page->metaDescription);
		Yii::app()->clientScript->registerMetaTag($page->seoKeyWords, 'keywords');
		$otherCriteria = new CDbCriteria([
			'condition' => 'regionId = :regionId',
			'params' => [':regionId' => $page->id],
			'limit' => 3,
			'order' => 'date desc'
		]);
		$this->render($this->page->type, [
			'route' => $page,
			'otherProjects' => Projects::model()->findAll($otherCriteria)
		]);
	}

	public function getPageId()
	{
		$page = $this->page->parent()->find();
		if($page->slug=='/')
			return $this->page->slug;
		else return $page->slug;
	}

	public function getCurrentPageId()
	{
		return $this->page->slug;
	}
}