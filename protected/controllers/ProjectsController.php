<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 01.08.2019
 * Time: 14:29
 */

class ProjectsController extends Controller
{
	public function actionIndex()
	{
		$this->render('index', [
			'projects' => Projects::model()->findAllByAttributes(['active' => 1]),
		]);
	}

	public function actionView($slug)
	{
		$otherCriteria = new CDbCriteria([
			'condition' => 'slug != :slug',
			'params' => [':slug' => $slug],
			'limit' => 3,
			'order' => 'date desc'
		]);
		$this->render('view', [
			'project' => Projects::model()->findByAttributes(['slug' => $slug]),
			'otherProjects' => Projects::model()->findAll($otherCriteria)
		]);
	}
}