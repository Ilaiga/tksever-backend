<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 06.08.2019
 * Time: 13:02
 */

class ServicesController extends Controller
{
	public function actionIndex()
	{
		$criteria = new CDbCriteria([
			'condition' => 'templateId = :tId',
			'params' => [':tId' => 6],
			'order' => 'lft desc'
		]);
		$this->render('index', ['services' => Pages::model()->findAll($criteria)]);
	}
}