<?php

class m190821_041110_pdfFile extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `deliveryRoutes`
	ADD COLUMN `pricePdf` VARCHAR(50) NULL DEFAULT NULL AFTER `markListsBg`;
');
	}

	public function down()
	{
		echo "m190821_041110_pdfFile does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}