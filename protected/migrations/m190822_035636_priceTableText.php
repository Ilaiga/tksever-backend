<?php

class m190822_035636_priceTableText extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `deliveryRoutes`
	ADD COLUMN `priceTableText` TEXT NULL DEFAULT NULL AFTER `pricePdf`;
');
	}

	public function down()
	{
		echo "m190822_035636_priceTableText does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}