<?php

class m190822_041430_priceTableTitle extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `deliveryRoutes`
	ADD COLUMN `priceTableTitle` VARCHAR(255) NULL AFTER `priceTableText`;
');
	}

	public function down()
	{
		echo "m190822_041430_priceTableTitle does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}