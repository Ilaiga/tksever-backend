<?php

class m190930_121305_update_delivery_table extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `deliveryRoutes`
	ADD COLUMN `whatDelivery` TEXT NULL DEFAULT NULL AFTER `priceTableTitle`;
');
	}

	public function down()
	{
		echo "m190930_121305_update_delivery_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}