<?php

Yii::import('application.models._base.BaseArticlesCategories');

class ArticlesCategories extends BaseArticlesCategories
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function listData() {
		$result = Yii::app()->db
			->createCommand("SELECT `categoryID`, `name` FROM `{$this->tableName()}` ORDER BY `name`")
			->queryAll();
		return CHtml::listData($result, 'categoryID', 'name');
	}
}