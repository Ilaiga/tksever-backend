<?php

Yii::import('application.models._base.BaseConfigParams');

class ConfigParams extends BaseConfigParams
{
	public $placeholder;
	public $value;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}