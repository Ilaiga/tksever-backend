<?php

Yii::import('application.models._base.BaseDeliveryRoutes');

use WebPConvert\WebPConvert;

class DeliveryRoutes extends BaseDeliveryRoutes
{
	const TYPE_REGION = 'region';
	const TYPE_ROUTE  = 'route';
	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_BG_WIDTH = 1920;
	const IMAGE_PREVIEW_WIDTH = 520;
	const IMAGE_EXTENSION = 'jpg';

	public $parentUrl;
	public $shortDescription;
	public $description;
	public $table;
	public $deliveryMethods;
	public $headerBlocks;
	public $markLists;
	public $image;
	public $pImage;
	public $markImage;
	public $priceFile;
	public $pricePdf;
	public $priceTableText;
	public $priceTableTitle;
	public $seoTextTitle;
	public $seoText;
	public $showParentTable;
	public $menuTitle;
	public $seoKeyWords;
	public $whatDelivery;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'nestedSetBehavior'=>array(
				'class'=> NestedSetBehavior::class,
				'leftAttribute'=>'lft',
				'rightAttribute'=>'rgt',
				'levelAttribute'=>'level',
				'rootAttribute'=>'root',
				'hasManyRoots'=>true,
			),
		);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image, pImage', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
			['priceFile', 'file', 'types' => 'pdf, xls, xlsx, doc, docx', 'allowEmpty' => true],
		]);
	}

	public function hasBgImage()
	{
		return empty($this->bgImage) ? true : false;
	}

	public function hasMarkImage()
	{
		return empty($this->markListsBg) ? true : false;
	}

	public function hasPreviewImage()
	{
		return empty($this->previewImage) ? true : false;
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	private function newFile($ext) {
		return uniqid() . '.' . $ext;
	}

	public function getFile() {
		if ($this->pricePdf) {
			return self::IMAGE_DIRECTORY . $this->pricePdf;
		}
	}

	public function getBgImage() {
		if ($this->bgImage) {
			return self::IMAGE_DIRECTORY . $this->bgImage;
		}
	}

	public function getMarkImage() {
		if ($this->markListsBg) {
			return self::IMAGE_DIRECTORY . $this->markListsBg;
		}
	}

	public function getPreviewImage() {
		if ($this->previewImage) {
			return self::IMAGE_DIRECTORY . $this->previewImage;
		}
	}

	public function getImageWebp() {
		if ($this->bgImage) {
			if(file_exists(getcwd() . self::IMAGE_DIRECTORY . $this->bgImage.'.webp'))
				return self::IMAGE_DIRECTORY . $this->bgImage.'.webp';
			else return false;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	protected function beforeDelete()
	{
		$this->deleteFiles(null);
		return parent::beforeDelete();
	}

	private function saveFile($img, $imageName, $size) {
		if ($img) {
			$temp_name = $img->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles($imageName);
			$this->$imageName  = $this->newImage();
			list($width, $height) = getimagesize($temp_name);
			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > $size ? $size : $width;
			$image
				->resize($width)
				->save(getcwd() . $directory . $this->$imageName, 70);
			WebPConvert::convert(getcwd() . $directory . $this->$imageName, getcwd() . $directory . $this->$imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function saveFiles() {
		if ($this->priceFile) {
			$temp_name = $this->priceFile->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles(null);
			$this->pricePdf  = $this->newFile($this->priceFile->getExtensionName());
			$this->priceFile->saveAs(getcwd() . $directory . $this->pricePdf);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
		$this->saveFile($this->image, 'bgImage', self::IMAGE_BG_WIDTH);
		$this->saveFile($this->pImage, 'previewImage', self::IMAGE_PREVIEW_WIDTH);
		$this->saveFile($this->markImage, 'markListsBg', self::IMAGE_BG_WIDTH);
	}

	private function deleteFiles($name) {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if(!empty($name))
			if ($this->$name) {
				@unlink(getcwd() . $directory . $this->$name);
				@unlink(getcwd() . $directory . $this->$name.'.webp');
			}
			if ($this->pricePdf)   @unlink(getcwd() . $directory . $this->pricePdf);
		}

	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles('bgImage');
			$this->deleteFiles('previewImage');
			$this->deleteFiles('markListsBg');
			$this->updateByPk($this->id, [
				'bgImage' => null,
			]);
		}
	}

	public function getUrl()
	{
		foreach ($this->ancestors()->findAll() as $item){
			if($item->slug != '/')
				$url[] = $item->slug;
		}
		$url[] = $this->slug;
		return '/'.implode('/', $url ?? []);
	}

	public function getTypes()
	{
		return [
			self::TYPE_REGION => 'Регион',
			self::TYPE_ROUTE => 'Направление'
		];
	}

	public function getTableArray()
	{
		if($this->showParentTable && $this->level > 2) {
			$this->table = $this->parent()->find()->table;
		}
		$table = json_decode($this->table, true);
		if(!empty($table['head'])) return $table;
	}

	public function getHeaderBlocks()
	{
		$list = json_decode($this->headerBlocks, true);
		if(empty($list[0]['text'])) return [];
		return $list;
	}

	public function getMarkLists()
	{
		$list =  json_decode($this->markLists, true);
		if(empty($list[0]['head'])) return [];
		foreach ($list as $item) {
			if(!empty($item['head'])){
				$result[] = $item;
			}
		}
		return $result ?? [];
	}

	public function getDeliveryMethodsArray()
	{
		$list = json_decode($this->deliveryMethods, true);
		if(empty($list[0]['name'])) return [];
		return $list;
	}

	public function getChildRoutes()
	{
		return $this->children()->findAll();
	}

	public function getRegions()
	{
		return self::model()->findAllByAttributes([], 'id != :selfId and level != 1', ['selfId' => $this->id]);
	}

	public function getOtherRoutes()
	{
		$crit = new CDbCriteria([
			'select' => '*, rand() as rand',
			'condition' => 'id != :selfId',
			'params' => ['selfId' => $this->id],
			'order' => 'rand'
		]);
		$parent = $this->parent()->find();
		return $parent->children()->findAll($crit);
	}

	public function getParentRoutes()
	{
		return $this->parent()->findAllByAttributes([], 'level != 1');
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('pages', 'Заголовок'),
			'menuTitle' => Yii::t('pages', 'Заголовок в меню'),
			'active' => Yii::t('pages', 'Активен'),
			'shortDescription' => Yii::t('pages', 'Краткое описание'),
			'description' => Yii::t('pages', 'Описание'),
			'image' => Yii::t('pages', 'Фоновое изображение'),
			'pImage' => Yii::t('pages', 'Изображение превью'),
			'markImage' => Yii::t('pages', 'Изображение фон у маркированных списков'),
			'table' => Yii::t('pages', 'Таблица'),
			'deliveryMethods' => Yii::t('pages', 'Способы доставки'),
			'type' => Yii::t('pages', 'Тип'),
			'priceFile' => Yii::t('pages', 'Файл с прайсом'),
			'priceTableText' => Yii::t('pages', 'Текс под таблицей с прайсом'),
			'priceTableTitle' => Yii::t('pages', 'Заголовок таблицы с прайсом'),
			'seoTextTitle' => Yii::t('pages', 'Заголовок сео текста'),
			'seoText' => Yii::t('pages', 'Сео текст'),
			'showOnMainPage' => Yii::t('pages', 'Показывать на главной странице'),
			'showParentTable' => Yii::t('pages', 'Показывать таблицу родительского раздела'),
			'seoKeyWords' => Yii::t('app', 'Keywords'),
			'whatDelivery' => Yii::t('app', 'Что перевозим'),
		];
	}

}