<?php

Yii::import('application.models._base.BasePages');

use WebPConvert\WebPConvert;

class Pages extends BasePages
{
	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_PREVIEW_WIDTH = 520;
	const IMAGE_EXTENSION = 'jpg';

	public $previewImage;
	public $previewImageFile;
	public $parentUrl;
	public $metaTitle;
	public $description;
	public $menuModel;
	public $content;
	public $showDeliveryTypeOnMainPage;
	public $menuTitle;
	public $seoKeyWords;


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['previewImage', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	public function behaviors()
	{
		return array(
			'nestedSetBehavior'=>array(
				'class'=> NestedSetBehavior::class,
				'leftAttribute'=>'lft',
				'rightAttribute'=>'rgt',
				'levelAttribute'=>'level',
			),
		);
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('pages', 'Заголовок'),
			'active' => Yii::t('pages', 'Активен'),
			'templateID' => Yii::t('pages', 'Шаблон'),
			'content' => Yii::t('pages', 'Контент'),
			'route' => Yii::t('pages', 'Route (маршрут к действию)'),
			'previewImageFile' => Yii::t('pages', 'Изображение (превью)'),
			'showDeliveryTypeOnMainPage' => Yii::t('pages', 'Показывать в блоке "виды перевозок"'),
			'menuTitle' => Yii::t('app', 'Заголовок в меню'),
			'seoKeyWords' => Yii::t('app', 'Keywords'),
		];
	}

	public function getUrl()
	{
		$url[] = $this->slug;
		foreach ($this->ancestors()->findAll() as $item){
			if($item->slug != '/')
			$url[] = $item->slug;
		}
		return '/'.implode('/', array_reverse($url) ?? []);
	}

	public function hasPreviewImage()
	{
		return empty($this->previewImage) ? true : false;
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	public function getPreviewImage() {
		if ($this->previewImage) {
			return self::IMAGE_DIRECTORY . $this->previewImage;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFile($img, $imageName, $size) {
		if ($img) {
			$temp_name = $img->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles($imageName);
			$this->$imageName  = $this->newImage();
			list($width, $height) = getimagesize($temp_name);
			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > $size ? $size : $width;
			$image
				->resize($width)
				->save(getcwd() . $directory . $this->$imageName, 70);
			WebPConvert::convert(getcwd() . $directory . $this->$imageName, getcwd() . $directory . $this->$imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function saveFiles() {
		$this->saveFile($this->previewImageFile, 'previewImage', self::IMAGE_PREVIEW_WIDTH);
	}

	private function deleteFiles($name) {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->$name) {
				@unlink(getcwd() . $directory . $this->$name);
				@unlink(getcwd() . $directory . $this->$name.'.webp');
			}
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles('previewImage');
			$this->updateByPk($this->id, [
				'previewImage' => null,
			]);
		}
	}


}