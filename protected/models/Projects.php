<?php

Yii::import('application.models._base.BaseProjects');

use WebPConvert\WebPConvert;

class Projects extends BaseProjects
{
	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_BG_WIDTH = 1920;
	const IMAGE_EXTENSION = 'jpg';

	public $tmpImage;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['tmpImage', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	public function hasImage()
	{
		return empty($this->image) ? true : false;
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	public function getImage() {
		if ($this->image) {
			return self::IMAGE_DIRECTORY . $this->image;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFile($img, $imageName, $size) {
		if ($img) {
			$temp_name = $img->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles($imageName);
			$this->$imageName  = $this->newImage();
			list($width, $height) = getimagesize($temp_name);
			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > $size ? $size : $width;
			$image
				->resize($width)
				->save(getcwd() . $directory . $this->$imageName, 70);
			WebPConvert::convert(getcwd() . $directory . $this->$imageName, getcwd() . $directory . $this->$imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function saveFiles() {
		$this->saveFile($this->tmpImage, 'image', self::IMAGE_BG_WIDTH);
	}

	private function deleteFiles($name) {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->$name) {
				@unlink(getcwd() . $directory . $this->$name);
				@unlink(getcwd() . $directory . $this->$name.'.webp');
			}
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles('image');
			$this->updateByPk($this->id, [
				'image' => null,
			]);
		}
	}

	public function getRegionList()
	{
		return
				[0 => ''] +
				CHtml::listData(DeliveryRoutes::model()->findAllByAttributes(['type' => 'route']), 'id', function ($data) {
					return $data->title;
				});
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('projects', 'Название проекта'),
			'active' => Yii::t('projects', 'Активен'),
			'description' => Yii::t('projects', 'Описание'),
			'image' => Yii::t('projects', 'Изображение'),
			'position' => Yii::t('projects', 'Позиция'),
			'goods' => Yii::t('projects', 'Груз'),
			'from' => Yii::t('projects', 'Откуда'),
			'to' => Yii::t('projects', 'Куда'),
			'customer' => Yii::t('projects', 'Заказчик'),
			'features' => Yii::t('projects', 'Особенности перевозки'),
			'date' => Yii::t('projects', 'Дата'),
			'regionId' => Yii::t('projects', 'Регион'),
		];
	}

	public function scopes()
	{
		return array(
			'sort'=>array(
				'order'=>'position ASC'
			),
		);
	}

}