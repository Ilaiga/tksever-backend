<?php

Yii::import('application.models._base.BaseRequests');

class Requests extends BaseRequests
{
	public $name;
	public $type;
	public $phone;
	public $email;
	public $comment;
	public $cargo;
	public $cargo_type;
	public $weight;
	public $capacity;
	public $departure;
	public $destination;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}