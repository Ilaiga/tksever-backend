<?php

Yii::import('application.models._base.BaseServices');

use WebPConvert\WebPConvert;

class Services extends BaseServices
{
	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 5242880;
	const IMAGE_BG_WIDTH = 1920;
	const IMAGE_PREVIEW_WIDTH = 520;
	const IMAGE_EXTENSION = 'jpg';

	public $parentUrl;
	public $dayPrice;
	public $monthPrice;
	public $halfMonthPrice;
	public $depositPrice;
	public $shortDescription;
	public $advancedDescription;
	public $description;
	public $image;
	public $pImage;


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function hasBgImage()
	{
		return empty($this->bgImage) ? true : false;
	}

	public function hasPreviewImage()
	{
		return empty($this->previewImage) ? true : false;
	}

	public function behaviors()
	{
		return array(
			'nestedSetBehavior'=>array(
				'class'=> NestedSetBehavior::class,
				'leftAttribute'=>'lft',
				'rightAttribute'=>'rgt',
				'levelAttribute'=>'level',
				'rootAttribute'=>'root',
				'hasManyRoots'=>true,
			),
		);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image, pImage', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	public function relations() {
		return array_merge(parent::relations(),
			[
				'priceItems' => [self::HAS_MANY, 'ServicePricesTable', 'serviceId', 'order' => 'position asc'],
				'sellPriceItems' => [self::HAS_MANY, 'ServiceSellPricesTable', 'serviceId', 'order' => 'position asc'],
				'activePriceItems' => [self::HAS_MANY, 'ServicePricesTable', 'serviceId', 'order' => 'position asc', 'condition' => 'active = 1'],
				'activeSellPriceItems' => [self::HAS_MANY, 'ServiceSellPricesTable', 'serviceId', 'order' => 'position asc', 'condition' => 'active = 1'],
			]);
	}

	public function getPriceTable()
	{
		$head = [
			'Наименование продукции',
			'Цена за сутки',
			'Цена за месяц',
			'Сумма залога',
		];
		foreach ($this->activePriceItems as $item){
			if(empty($item->depositPrice)) unset($head[3]);
			$body[] = [
				$item->name, $item->dayPrice, $item->monthPrice, $item->depositPrice
			];
		}
		$result = [
			'head' => $head,
			'body' => array_map(function (&$a) {if(empty($a[3])) unset($a[3]);return $a;}, $body ?? [])
		];
		return $result;
	}

	public function getSellPriceTable()
	{
		$head = [
			'Наименование продукции',
			'Цена руб. (б/у)',
			'Цена руб. (новые)',
			'Вес, кг',
		];
		foreach ($this->activeSellPriceItems as $item){
			if(empty($item->weight)) unset($head[3]);
			$body[] = [
				$item->name, $item->usedPrice, $item->newPrice, $item->weight
			];
		}
		$result = [
			'head' => $head,
			'body' => array_map(function (&$a) {if(empty($a[3])) unset($a[3]);return $a;}, $body ?? [])
		];
		return $result;
	}

	private function newImage() {
		return uniqid() . '.' . self::IMAGE_EXTENSION;
	}

	public function getImage() {
		if ($this->bgImage) {
			return self::IMAGE_DIRECTORY . $this->bgImage;
		}
	}

	public function getPreviewImage() {
		if ($this->previewImage) {
			return self::IMAGE_DIRECTORY . $this->previewImage;
		}
	}

	public function getImageWebp() {
		if ($this->bgImage) {
			if(file_exists(getcwd() . self::IMAGE_DIRECTORY . $this->bgImage.'.webp'))
				return self::IMAGE_DIRECTORY . $this->bgImage.'.webp';
			else return false;
		}
	}

	public function afterSave() {
		$this->saveFiles();
	}

	private function saveFile($img, $imageName, $size) {
		if ($img) {
			$temp_name = $img->getTempName();
			$directory = self::IMAGE_DIRECTORY;
			$this->deleteFiles($imageName);
			$this->$imageName  = $this->newImage();
			list($width, $height) = getimagesize($temp_name);
			$image = new EasyImage($temp_name, 'Imagick');
			$width = $width > $size ? $size : $width;
			$image
				->resize($width)
				->save(getcwd() . $directory . $this->$imageName, 70);
			WebPConvert::convert(getcwd() . $directory . $this->$imageName, getcwd() . $directory . $this->$imageName.'.webp', [
				'quality' => 'auto',
				'max-quality' => 70,
			]);
			$this->isNewRecord = false;
			$this->saveAttributes($this->attributes);
		}
	}

	private function saveFiles() {
		$this->saveFile($this->image, 'bgImage', self::IMAGE_BG_WIDTH);
		$this->saveFile($this->pImage, 'previewImage', self::IMAGE_PREVIEW_WIDTH);
	}

	private function deleteFiles($name) {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->$name) {
				@unlink(getcwd() . $directory . $this->$name);
				@unlink(getcwd() . $directory . $this->$name.'.webp');
			}
		}
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles('bgImage');
			$this->deleteFiles('previewImage');
			$this->updateByPk($this->id, [
				'bgImage' => null,
			]);
		}
	}

	public function getUrl()
	{
		foreach ($this->ancestors()->findAll() as $item){
			if($item->slug != '/')
				$url[] = $item->slug;
		}
		$url[] = $this->slug;
		return '/'.implode('/', $url ?? []);
	}

	public function getServicePriceItems()
	{
		$r = [];
		if($this->dayPrice) {
			$r[] = [
				'title' => 	$this->getAttributeLabel('dayPrice'),
				'price' => $this->dayPrice
			];
		}
		if($this->monthPrice) {
			$r[] = [
				'title' => 	$this->getAttributeLabel('monthPrice'),
				'price' => $this->monthPrice
			];
		}
		if($this->halfMonthPrice) {
			$r[] = [
				'title' => 	$this->getAttributeLabel('halfMonthPrice'),
				'price' => $this->halfMonthPrice
			];
		}
		if($this->depositPrice) {
			$r[] = [
				'title' => 	$this->getAttributeLabel('depositPrice'),
				'price' => $this->depositPrice
			];
		}
		return $r ?? [];
	}

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('pages', 'Заголовок'),
			'menuTitle' => Yii::t('pages', 'Заголовок в меню'),
			'active' => Yii::t('pages', 'Активен'),
			'shortDescription' => Yii::t('pages', 'Краткое описание'),
			'advancedDescription' => Yii::t('pages', 'Описание 2'),
			'description' => Yii::t('pages', 'Описание'),
			'dayPrice' => Yii::t('pages', 'Цена за день'),
			'monthPrice' => Yii::t('pages', 'Цена за месяц'),
			'halfMonthPrice' => Yii::t('pages', 'Цена за 15 дней'),
			'depositPrice' => Yii::t('pages', 'Цена залога'),
			'image' => Yii::t('pages', 'Изображение'),
			'pImage' => Yii::t('pages', 'Изображение в блоке услуг'),
			'showOnServicesBlock' => Yii::t('pages', 'Показывать в блоке услуг'),
			'showOnServicesBlockOnMainPage' => Yii::t('pages', 'Показывать в блоке услуг на главной'),
			'dontShowOnMenu' => Yii::t('pages', 'Не показывать в меню'),
		];
	}

}