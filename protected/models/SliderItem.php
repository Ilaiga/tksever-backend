<?php

Yii::import('application.models._base.BaseSliderItem');

use WebPConvert\WebPConvert;

class SliderItem extends BaseSliderItem
{
	const IMAGE_DIRECTORY = '/files/';
	const IMAGE_MAX_SIZE = 6242880;
	const IMAGE_BIG_WIDTH = 1920;
	const IMAGE_LARGE_WIDTH = 1240;

	protected $imageExtension = 'png';

	public $image;
	public $position;
	public $active;
	public $title;
	public $content;
	public $imageLarge;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image, imageLarge', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	private function newImage() {
		return uniqid() . '.' . $this->imageExtension;
	}

	public function getImage() {
		if ($this->imageName) {
			return self::IMAGE_DIRECTORY . $this->imageName;
		}
	}

	public function getLargeImage() {
		if ($this->imageLargeName) {
			return self::IMAGE_DIRECTORY . $this->imageLargeName;
		}
	}

	public function afterSave() {
		if(!empty($this->image))
			$this->saveFiles();
		return parent::afterSave();
	}

	private function saveFiles() {
		$temp_name = $this->image->getTempName();
//		$ltemp_name = $this->imageLarge->getTempName();
		$directory = self::IMAGE_DIRECTORY;

		$this->deleteFiles();
		$this->imageName  = $this->newImage();
//		$this->imageLargeName  = $this->newImage();

		list($width, $height) = getimagesize($temp_name);
//		list($lwidth, $lheight) = getimagesize($ltemp_name);

		$image = new EasyImage($temp_name, 'Imagick');
//		$limage = new EasyImage($ltemp_name);
		$width = $width > self::IMAGE_BIG_WIDTH ? self::IMAGE_BIG_WIDTH : $width;
//		$lwidth = $lwidth > self::IMAGE_LARGE_WIDTH ? self::IMAGE_LARGE_WIDTH : $lwidth;

		$image
			->resize($width)
			->save(getcwd() . $directory . $this->imageName, 70);

//		$limage
//			->resize($lwidth)
//			->save(getcwd() . $directory . $this->imageLargeName);

		WebPConvert::convert(getcwd() . $directory . $this->imageName, getcwd() . $directory . $this->imageName.'.webp', [
			'quality' => 'auto',
			'max-quality' => 80,
		]);
//
//		WebPConvert::convert(getcwd() . $directory . $this->imageLargeName, getcwd() . $directory . $this->imageLargeName.'.webp', [
//			'quality' => 'auto',
//			'max-quality' => 80,
//		]);

		$this->isNewRecord = false;
		$this->saveAttributes($this->attributes);
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->imageName)   @unlink(getcwd() . $directory . $this->imageName);
			if ($this->imageName)   @unlink(getcwd() . $directory . $this->imageName.'.webp');
			if ($this->imageLargeName)   @unlink(getcwd() . $directory . $this->imageLargeName);
			if ($this->imageLargeName)   @unlink(getcwd() . $directory . $this->imageLargeName.'.webp');
		}
	}

	public function beforeDelete()
	{
		$this->deleteImages();
		return parent::beforeDelete();
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles();
			$this->updateByPk($this->id, [
				'imageName' => null,
				'imageLargeName' => null
			]);
		}
	}

	public function getTemplates()
	{
		return [
			'trust' => 'Нам доверяют',
		];
	}
}