<?php

/**
 * This is the model base class for the table "configGroups".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ConfigGroups".
 *
 * Columns in table "configGroups" available as properties of the model,
 * followed by relations of table "configGroups" available as properties of the model.
 *
 * @property integer $id
 * @property integer $parentID
 * @property string $name
 * @property string $description
 *
 * @property ConfigGroups $parent
 * @property ConfigGroups[] $configGroups
 * @property ConfigParams[] $configParams
 */
abstract class BaseConfigGroups extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'configGroups';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'ConfigGroups|ConfigGroups', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('parentID, name, description', 'required'),
			array('parentID', 'numerical', 'integerOnly'=>true),
			array('name, description', 'length', 'max'=>255),
			array('id, parentID, name, description', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'parent' => array(self::BELONGS_TO, 'ConfigGroups', 'parentID'),
			'configGroups' => array(self::HAS_MANY, 'ConfigGroups', 'parentID'),
			'configParams' => array(self::HAS_MANY, 'ConfigParams', 'groupID'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'parentID' => null,
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'parent' => null,
			'configGroups' => null,
			'configParams' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parentID', $this->parentID);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}