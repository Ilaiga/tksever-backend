<?php

/**
 * This is the model base class for the table "reviews".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Reviews".
 *
 * Columns in table "reviews" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property integer $position
 * @property integer $active
 * @property string $name
 * @property string $company
 * @property string $text
 *
 */
abstract class BaseReviews extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'reviews';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Reviews|Reviews', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('name, company, text', 'required'),
			array('position, active', 'numerical', 'integerOnly'=>true),
			array('name, company', 'length', 'max'=>255),
			array('position, active', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, position, active, name, company, text', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'position' => Yii::t('app', 'Position'),
			'active' => Yii::t('app', 'Active'),
			'name' => Yii::t('app', 'Name'),
			'company' => Yii::t('app', 'Company'),
			'text' => Yii::t('app', 'Text'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('position', $this->position);
		$criteria->compare('active', $this->active);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('company', $this->company, true);
		$criteria->compare('text', $this->text, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}