<?php

Yii::import('zii.widgets.CBreadcrumbs');

class ABreadcrumbs extends CBreadcrumbs
{
	public $tagName = 'ul';
	public $htmlOptions = ['id' => 'breadcrumbs', 'class' => 'breadcrumb'];
	public $encodeLabel = true;
	public $homeLink = '<li><i class="icon-home"></i><a href="/admin/">Панель управления</a></li>';
	public $links = [];
	public $activeLinkTemplate = '<li class="current"><a href="{url}">{label}</a></li>';
	public $inactiveLinkTemplate = '<li><a href="{url}">{label}</a></li>';
	public $separator = '';
}