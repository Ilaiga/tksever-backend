<?php

class ALinkPager extends CLinkPager
{
	const CSS_SELECTED_PAGE = 'active';
	const CSS_DISABLED_PAGE = 'disabled';

	public function init() {
		$this->htmlOptions = ['class' => 'pagination', 'id' => ''];
		$this->nextPageLabel = 'вперед &rarr;';
		$this->prevPageLabel = '&larr; назад';
//		$this->header = self::getHeader();
//		$this->footer = self::getFooter();
		parent::init();
	}

	protected static function getHeader() {
		return '<div>';
	}

	protected static function getFooter() {
		return '</div>';
	}

	protected function createPageButton($label, $page, $class, $disabled, $selected) {
		if ($selected || $disabled)
			$class .= ' ' . ($disabled ? self::CSS_DISABLED_PAGE : self::CSS_SELECTED_PAGE);

		return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
	}

	protected function createPageButtons() {
		if (($pageCount = $this->getPageCount()) <= 1)
			return array();

		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons = array();

		// prev page
		if (($page = $currentPage - 1) < 0)
			$page = 0;
		$buttons[] = $this->createPageButton($this->prevPageLabel, $page, null, $currentPage <= 0, false);

		// internal pages
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$buttons[] = $this->createPageButton($i + 1, $i, null, false, $i == $currentPage);
		}

		// next page
		if (($page = $currentPage + 1) >= $pageCount - 1)
			$page = $pageCount - 1;
		$buttons[] = $this->createPageButton($this->nextPageLabel, $page, null, $currentPage >= $pageCount - 1, false);

		return $buttons;
	}
	
	public function run() {
//		echo $this->header;
		echo CHtml::tag('ul', $this->htmlOptions, implode("\n", $this->createPageButtons()));
//		echo $this->footer;
	}
}