<?php
Yii::import('application.modules.admin.components.InfoblockProviderData');
class FooterMenu implements InfoblockProviderData
{
	private $params;

	public function __construct($params)
	{
		$this->params = $params;
	}

	public function getData() : array
	{
		if (($cache = Yii::app()->cache) !== null && ($val = $cache->get('footerMenu'.date('Y-m-d'))) !== false && $this->params['useCache']) {
			return $val;
		} else {
			$mainPage = Pages::model()->roots()->find();
			foreach ($mainPage->children()->findAll() as $headPage) {
				$items = $headPage->children()->findAll();
				if (!empty($items)) {
					foreach ($items as $item) {
						$result[$headPage->title][] = [
							'name' => $item->title,
							'url' => $item->url,
						];
					}
				} else {
					$result[$mainPage->title][] = [
						'name' => $headPage->title,
						'url' => $headPage->url,
					];
				}

			}
			$cache->set('footerMenu'.date('Y-m-d'), $result ?? [], 3600 * 24);
			return $result ?? [];
		}
	}
}