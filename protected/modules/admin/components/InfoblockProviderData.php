<?php

interface InfoblockProviderData
{
	public function getData() : array;
}