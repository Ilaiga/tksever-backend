<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 23.04.2019
 * Time: 16:27
 */

class MenuHelperComponent extends CApplicationComponent
{
	public $useCache = true;
	public $parent = 0;
	protected $requestUrl;

	public function __construct()
	{
		$this->requestUrl = Yii::app()->request->requestUri;
	}

	public function getMainMenu()
	{
		$root = Pages::model()->findByAttributes(['id' => 1]);
		foreach ($root->children()->findAllByAttributes(['active' => 1], ['order' => 'lft asc']) as $item) {
			$result[] = [
				'title' => $item->menuTitle ? $item->menuTitle : $item->title,
				'url' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'showSearchBar' => $item->menuModel ? true : false,
				'subMenu' => $item->menuModel
					? $this->menuTree($item->menuModel::model()->findByAttributes(['id' => 1]), ['showMoreDirections' => true])
					: $this->menuTree($item)
			];
		}
		return $result;
	}

	protected function menuTree($obj, $params = [])
	{
		if(!($obj = $obj->children()->findAllByAttributes(['active' => 1], ['order' => 'lft asc']))) return false;
		foreach ($obj as $item) {
			$result[] = [
				'title' => $item->menuTitle ? $item->menuTitle : $item->title,
				'url' => $item->url == $this->requestUrl ? 'javascript:;' : $item->url,
				'subMenu' => $this->menuTree($item)
			]+$params;
		}
		return $result ?? [];
	}

//	public function getChildPagesList($parent)
//	{
//		$category=Pages::model()->findByAttributes(['id' => $parent]);
//		foreach ($category->children()->findAllByAttributes(['active' => 1], ['order' => 'lft asc']) as $item) {
//			$result[] = [
//				'name' => $item->title,
//				'text' => $item->title,
//				'url' => $item->url,
//				'link' => $item->url,
//				'slug' => $item->slug,
//			];
//		}
//		return $result ?? [];
//	}
//
//	public function getChildServiceList($parent)
//	{
//		$category = Services::model()->findByAttributes(['id' => $parent]);
//		if (!$category) return [];
//		foreach ($category->children()->findAllByAttributes(['active' => 1, 'dontShowOnMenu' => 0], ['order' => 'lft asc']) as $item) {
//			$result[] = [
//				'name' => $item->menuTitle,
//				'text' => $item->menuTitle,
//				'url' => $item->url,
//				'link' => $item->url,
//				'slug' => $item->slug,
//			];
//		}
//		return $result ?? [];
//	}

}