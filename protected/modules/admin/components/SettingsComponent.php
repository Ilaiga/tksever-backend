<?php

class SettingsComponent extends CApplicationComponent
{
	public $useCache = true;

	public function get($name)
	{
		if (($cache = Yii::app()->cache) !== null
			&& ($val = $cache->get(Yii::app()->getBaseUrl(true).':SettingsComponent:' . $name)) !== false && $this->useCache) {
			return $val;
		} else {
			$param = self::getParamModel($name);
			if ($param !== null) {
				if($this->useCache) {
					$cache->set(Yii::app()->getBaseUrl(true).':SettingsComponent:' . $name, $param, 3600 * 2);
				}
				return $param;
			}
			return '';
		}
	}

	private static function getParamModel($path)
	{
		$params = explode(':', $path);
		$option = array_pop($params);
		$parentId  = 1;
		foreach ($params as $param) {
			$parentId = ConfigGroups::model()->findByAttributes(['name' => $param, 'parentID' => $parentId])->id;;
		}
		return ConfigParams::model()->findByAttributes(['name' => $option, 'groupID' => $parentId])->value;
	}

	public function getTree()
	{
		return ConfigGroups::model()->getTree()->getChild()[0];
	}
}