<?php

class CacheController extends AController
{
	public function actionIndex()
	{
		Yii::app()->cache->flush();
		$this->redirect('/admin');
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}