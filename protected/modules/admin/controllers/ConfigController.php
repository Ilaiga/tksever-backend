<?php

class ConfigController extends AController
{
	public function actionIndex($groupId)
	{
		$groups = ConfigGroups::model()->getTree();
		$post = Yii::app()->request->getPost('ConfigParams');
		if(!empty($post)) {
			foreach ($post as $id => $attr) {
				$model = ConfigParams::model()->findByPk($id);
				$model->setAttributes($attr);
				$model->save();
			}
			Yii::app()->user->setFlash('success', 'Данные обновлены');
			$this->refresh();
		}
		$this->render('index', ['items' => $groups->getChildById($groupId)]);
	}

	public function actionUpdateParam()
	{
		$post = Yii::app()->request->getPost('param');
		$model = ConfigParams::model()->findByPk($post['id']);
		$model->setAttributes($post);
		if($model->save()) {
			echo CJSON::encode(['status' => 'ok']);
		} else {
			echo CJSON::encode(['status' => 'fail', 'errors' => $model->getErrors()]);
		}
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}