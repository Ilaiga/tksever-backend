<?php

class DefaultController extends AController
{
	public function actionIndex()
	{
		$this->setPageTitle('Админ панель');
		$this->render('index');
	}

	public function actionLogin()
	{
		$this->setPageTitle('Авторизация');
		$form = new LoginForm();
		if(($data = Yii::app()->request->getPost('LoginForm'))) {
			$form->setAttributes($data);
			if($form->validate()) {
				$identity = new UserIdentity($form->login, $form->password);
				if($identity->authenticate()) {
					Yii::app()->user->login($identity);
					$this->redirect('/admin');
				}
			}
		}
		$this->render('index', ['form' => $form, 'error' => $identity->errorMessage ?? '']);
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('/admin');
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['allow', 'actions'=> ['login'], 'users'=> ['*']],
			['deny', 'users'=> ['*']]
		];
	}

}