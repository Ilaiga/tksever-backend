<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 18.04.2019
 * Time: 14:20
 */

class DeliveryRoutesController extends AController
{
	protected $templates = [
		'1' => 'service_form',
		'35' => 'brand_form'
	];

	public function actionIndex()
	{
		$model = DeliveryRoutes::model()->roots()->findAll();
		$this->render('index', ['DeliveryRoutes' => $model]);
	}

	public function actionAdd($root = null)
	{
		$model = new DeliveryRoutes();
		if(!empty($root)) {
			$root = DeliveryRoutes::model()->findByPk($root);
			$model->parentUrl = $this->getParentUrl($root);
		}
		if(Yii::app()->request->getPost('DeliveryRoutes')) {
			$model->attributes = Yii::app()->request->getPost('DeliveryRoutes');
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->pImage = CUploadedFile::getInstance($model, 'pImage');
			$model->markImage = CUploadedFile::getInstance($model, 'markImage');
			$model->priceFile = CUploadedFile::getInstance($model, 'priceFile');
			if(!$root) {
				if($model->saveNode()) {
					Yii::app()->user->setFlash('success', 'Данные добавлены');
					$this->redirect($this->createUrl('/admin/DeliveryRoutes'));
				}
			} else {
				$model->validate();
				if($model->appendTo($root)) {
					Yii::app()->user->setFlash('success', 'Данные добавлены');
					$this->redirect($this->createUrl('/admin/DeliveryRoutes'));
				}
			}
		}
		$this->render($this->templates[$root->root], ['service' => $model, 'title' => 'Новая услуга']);
	}

	public function actionEdit($root)
	{
		$error = false;
		$model = DeliveryRoutes::model()->findByPk($root);
		if($root != 1)
			$model->parentUrl = $this->getParentUrl($model->parent()->find());
		if(Yii::app()->request->getPost('DeliveryRoutes')) {
			$model->attributes = Yii::app()->request->getPost('DeliveryRoutes');
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->pImage = CUploadedFile::getInstance($model, 'pImage');
			$model->markImage = CUploadedFile::getInstance($model, 'markImage');
			$model->priceFile = CUploadedFile::getInstance($model, 'priceFile');
			if($model->saveNode() && !$error) {
				Yii::app()->user->setFlash('success', 'Данные обновлены');
				$this->redirect($this->createUrl('/admin/DeliveryRoutes'));
			}
		}
		$this->render($this->templates[$model->root], ['service' => $model, 'title' => 'Редактирование региона']);
	}

	public function actionDelete($root)
	{
		$model = DeliveryRoutes::model()->findByPk($root);
		if($model->children()->find()){
			Yii::app()->user->setFlash('error', 'Невозможно удалить элемент имеющий дочерние блоки.');
			$this->redirect($this->createUrl('/admin/DeliveryRoutes/'));
		}
		try {
//			if($model->getImagePath())
//				unlink($model->getImagePath());
			DeliveryRoutes::model()->deleteByPk($root);
			Yii::app()->user->setFlash('success', 'Элемент удален');
		} catch (Exception $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
		}
		$this->redirect($this->createUrl('/admin/DeliveryRoutes/'));
	}

	public function actionChangePosition()
	{
		$request = Yii::app()->request->getPost('pos');
		$element = DeliveryRoutes::model()->findByPk($request['id']);
		$target = DeliveryRoutes::model()->findByPk($request['targetId']);
		switch ($request['action']) {
			case 'before':
				$element->moveBefore($target);
				break;
			case 'after':
				$element->moveAfter($target);
				break;
			case 'append':
				$element->moveAsFirst($target);
				break;
		}
	}

	private function getParentUrl(DeliveryRoutes $service = null)
	{
		if(empty($service)) return '/';
		$ancestors = $service->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentService) {
			$url[] = $parentService->slug;
		}
		$url[] = $service->slug;
		if(count($url) == 1) return '/';
		array_shift($url);
		return '/' . implode('/', $url) . '/';
	}

	public function actionDeleteFile($id)
	{
		if(Yii::app()->request->isAjaxRequest){
			$model = DeliveryRoutes::model()->findByPk($id);
			$model->pricePdf = null;
			$model->saveNode();
		}
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}