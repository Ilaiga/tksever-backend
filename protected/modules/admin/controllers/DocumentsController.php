<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 20.06.2019
 * Time: 13:20
 */

class DocumentsController extends AController
{
	public function actionIndex()
	{
		$this->render('index', ['documents' => Documents::model()->findAll()]);
	}

	public function actionCreate() {
		$model = new Documents();
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('Documents'));
			$model->fileTmp = CUploadedFile::getInstance($model, 'fileTmp');
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->redirect(['update', 'id' => $model->id]);
			}
		}
		$this->render('create', [
			'document' => $model,
		]);
	}

	public function actionUpdate() {
		$model = $this->loadRecord('Documents');
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('Documents'));
			$model->fileTmp = CUploadedFile::getInstance($model, 'fileTmp');
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('failed', true);
			}
		}

		$this->render('update', [
			'document' => $model
		]);
	}

	public function actionDelete() {
		$article = $this->loadRecord('Documents');
		$article->delete();
		Yii::app()->controller->redirect(Yii::app()->request->urlReferrer);
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}