<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 19.06.2019
 * Time: 17:06
 */

class FaqController extends AController
{
	public function actionIndex()
	{
		$this->render('index', ['questions' => Faq::model()->findAll()]);
	}

	public function actionCreate() {
		$model = new Faq();
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('Faq'));
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->redirect(['update', 'id' => $model->id]);
			}
		}
		$this->render('create', [
			'faq' => $model,
		]);
	}

	public function actionUpdate() {
		$model = $this->loadRecord('Faq');
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('Faq'));
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('failed', true);
			}
		}

		$this->render('update', [
			'faq' => $model
		]);
	}

	public function actionDelete() {
		$article = $this->loadRecord('Faq');
		$article->delete();
		Yii::app()->controller->redirect(Yii::app()->request->urlReferrer);
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}