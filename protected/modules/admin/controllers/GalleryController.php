<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 20.06.2019
 * Time: 15:08
 */

class GalleryController extends AController
{
	public function actionIndex()
	{
		$this->render('index', ['images' => GalleryImage::model()->findAllByAttributes([], ['order' => 'position asc'])]);
	}

	public function actionCreate() {
		$model = new GalleryImage();
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('GalleryImage'));
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->redirect(['update', 'id' => $model->id]);
			}
		}
		$this->render('create', [
			'image' => $model,
		]);
	}

	public function actionUpdate() {
		$model = $this->loadRecord('GalleryImage');
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('GalleryImage'));
			$model->image = CUploadedFile::getInstance($model, 'image');
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('failed', true);
			}
		}

		$this->render('update', [
			'image' => $model
		]);
	}

	public function actionDelete() {
		$article = $this->loadRecord('GalleryImage');
		$article->delete();
		Yii::app()->controller->redirect(Yii::app()->request->urlReferrer);
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}
}