<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 29.04.2019
 * Time: 23:52
 */

class ProjectsController extends AController
{
	public function actionIndex()
	{
		$data = Projects::model()->findAllByAttributes([], ['order' => 'position asc']);
		$this->render('index', ['projects' => $data]);
	}

	public function actionDelete()
	{
		Projects::model()->findByPk(Yii::app()->request->getParam('id'))->delete();
		$this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionUpdate()
	{
		$id = Yii::app()->request->getParam('id');
		if(!$id) {
			$project = new Projects();
		} else {
			$project = Projects::model()->findByPk($id);
		}

		if(($post = Yii::app()->request->getPost('Projects'))) {
			$project->setAttributes($post);
			$project->tmpImage = CUploadedFile::getInstance($project, 'tmpImage');
			if($project->validate()) {
				$project->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$this->redirect(Yii::app()->request->urlReferrer);
			} else {
				Yii::app()->user->setFlash('error', true);
			}
		}
		$this->render('add', ['item' => $project]);
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}
