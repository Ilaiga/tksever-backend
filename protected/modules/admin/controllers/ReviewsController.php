<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 29.04.2019
 * Time: 23:52
 */

class ReviewsController extends AController
{
	public function actionIndex()
	{
		$data = Reviews::model()->findAllByAttributes([], ['order' => 'position asc']);
		$this->render('index', ['reviews' => $data]);
	}

	public function actionDelete()
	{
		Reviews::model()->findByPk(Yii::app()->request->getParam('id'))->delete();
		$this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionUpdate()
	{
		$id = Yii::app()->request->getParam('id');
		if(!$id) {
			$review = new Reviews();
		} else {
			$review = Reviews::model()->findByPk($id);
		}

		if(($post = Yii::app()->request->getPost('Reviews'))) {
			$review->setAttributes($post);
			if($review->validate()) {
				$review->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$this->redirect(Yii::app()->request->urlReferrer);
			} else {
				Yii::app()->user->setFlash('error', true);
			}
		}
		$this->render('add', ['item' => $review]);
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}
