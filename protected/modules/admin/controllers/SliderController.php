<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 17.05.2019
 * Time: 11:48
 */

class SliderController extends AController
{
	public function actionIndex()
	{
		$categories = SliderCategories::model()->findAll();
		$this->render('index', ['categories' => $categories]);
	}

	public function actionCategory($id)
	{
		$category = SliderCategories::model()->findByPk($id);
		$this->render('category', ['category' => $category]);
	}

	public function actionEditItem($id)
	{
		$item = SliderItem::model()->findByPk($id);
		if (Yii::app()->request->isPostRequest) {
			$item->setAttributes(Yii::app()->request->getPost('SliderItem'));
			$item->image = CUploadedFile::getInstance($item, 'image');
			$item->imageLarge = CUploadedFile::getInstance($item, 'imageLarge');
			$item->validate();
			if ($item->hasErrors() == false) {
				$item->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Изменения сохранены.'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('error', true);
			}
		}
		$this->render('edit', ['item' => $item, 'categoryId' => $item->category->id, 'category' => $item->category]);
	}

	public function actionNewItem($id)
	{
		$category = SliderCategories::model()->findByPk($id);
		$item = new SliderItem();
		if (Yii::app()->request->isPostRequest) {
			$item->setAttributes(Yii::app()->request->getPost('SliderItem'));
			$item->image = CUploadedFile::getInstance($item, 'image');
			$item->imageLarge = CUploadedFile::getInstance($item, 'imageLarge');
			$item->categoryId = $category->id;
			$item->validate();
			if ($item->hasErrors() == false) {
				$item->save($runValidation = false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Слайд добавлен.'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('error', true);
			}
		}
		$this->render('edit', ['item' => $item, 'categoryId' => $category->id, 'category' => $category]);
	}

	public function actionDeleteItem($id)
	{
		SliderItem::model()->deleteByPk($id);
		$this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionDeleteImage()
	{
		$id = Yii::app()->request->getParam('id');
		$item = SliderItem::model()->findByAttributes(['id' => $id]);
		$item->deleteImages();
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['*']]
		];
	}

}