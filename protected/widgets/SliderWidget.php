<?php

class SliderWidget extends CWidget {

	public $sliderId;
	public $params = [];
	public $view = 'base';

	public function run() {
		$slider = SliderCategories::model()->findByPk($this->sliderId);
		$this->render('slider/'.$this->view , ['slider' => $slider, 'params' => $this->params]);
	}

}